clear all; close all; clc;
addpath bin/

%% Inputs

load sphere.mat
Activity = 10e6; % Bq/cc
TumorVolume = 40; % cc
TotalActivity = Activity * TumorVolume; % Bq

WorldWidth = 1500; % mm 

%% Source image 


img = double (img);
img = img - min(img(:));
img = img ./ max(img(:));
img = img .* 65000;
img = uint16 (img);


figure, imshow3D(img,[]);

[xSize, ySize, zSize] = size(img);

fID1 = fopen('sphere.raw','w');
fwrite(fID1, img, 'uint16');
fclose(fID1);


%% Activity image
Sum = sum(img(:));
[Counts, GrayLevels] = imhist(img,(2^16));
Histogram(:,1) = GrayLevels;
Histogram(:,2) = Counts;
nTumorVoxels = sum(Counts(2:size(Counts)));

Counter1 = 0;
for i1 = 1:size(Counts)
    if Counts(i1) ~= 0 && GrayLevels(i1) ~= 0
        Counter1 = Counter1 + 1;
    end
end

fID2 = fopen('sphereAR.dat','w');
fprintf (fID2, '%i\n', Counter1);
Counter2 = 2;

for i2 = 1:size(Counts)
    if Counts(i2) ~= 0 && GrayLevels(i2) ~= 0
        ActivityChart (Counter2-1,1) = Histogram(i2,1);
        ActivityChart (Counter2-1,2) = (TotalActivity * Histogram(i2,1)) / Sum;
        Histogram (i2,3) = (TotalActivity * Histogram(i2,1)) / Sum;
        fprintf(fID2, '%i %i %f\n', Histogram(i2,1), Histogram(i2,1), Histogram(i2,3));
        Counter2 = Counter2 + 1;
    end
end
fclose(fID2);
ActualTotalActivtity = sum(Histogram(:,2) .* Histogram(:,3));
VoxelVolume = ((TumorVolume*1000)/nTumorVoxels); % mm3
VoxelSize_mm = VoxelVolume ^ (1/3);

VoxelizedSourceVolumeWidth = VoxelSize_mm * xSize;
OffsetForGATE = -(VoxelizedSourceVolumeWidth/2)

%% Source header file

fID3 = fopen('SourceImage.mhd','w');
    fprintf(fID3, 'ObjectType = Image\n');
    fprintf(fID3, 'NDims = 3\n');
    fprintf(fID3, 'BinaryData = True\n');
    fprintf(fID3, 'BinaryDataByteOrderMSB = False\n');
    fprintf(fID3, 'CompressedData = False\n');
    fprintf(fID3, 'TransformMatrix = 1 0 0 0 1 0 0 0 1\n');
    fprintf(fID3, 'Offset = 0 0 0\n');
    fprintf(fID3, 'CenterOfRotation = 0 0 0\n');
    fprintf(fID3, 'AnatomicalOrientation = RAI\n');
    fprintf(fID3, 'ElementSpacing = %f %f %f\n',VoxelSize_mm, VoxelSize_mm, VoxelSize_mm );
    %fprintf(fID3, 'ElementSize = %f %f %f\n',VoxelSize_mm, VoxelSize_mm, VoxelSize_mm );
    fprintf(fID3, 'ITK_InputFilterName = MetaImageIO\n');
    fprintf(fID3, 'DimSize = %i %i %i\n',xSize, ySize, zSize );
    fprintf(fID3, 'ElementType = MET_SHORT\n');
    fprintf(fID3, 'ElementDataFile = SourceImage.raw\n');
fclose(fID3);