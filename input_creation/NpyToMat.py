import numpy as np
import scipy.io

img = np.load('volume.npy',allow_pickle=True)
mdic = {"img": img, "label": "sphere_experiment"}
scipy.io.savemat('sphere.mat', mdic)