import numpy as np
import scipy.io

img = np.load('cylinder_with_hollow_sphere.npy',allow_pickle=True)
mdic = {"img": img, "label": "cylinder_with_hollow_sphere_experiment"}
scipy.io.savemat('cylinder_with_hollow_sphere.mat', mdic)