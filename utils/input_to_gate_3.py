import numpy as np


# Parameters
cylinder_radius = 50
cylinder_height = 200
sphere_radius = 45

# Define the size of the grid
size_x = cylinder_radius * 2    
size_y = cylinder_radius * 2
size_z = cylinder_height

# Create a 3D grid of coordinates
x, y, z = np.ogrid[:size_x, :size_y, :size_z]

# Center the cylinder in the grid
center_x, center_y, center_z = cylinder_radius, cylinder_radius, size_z // 2

# Create the cylinder
cylinder = ((x - center_x)**2 + (y - center_y)**2) <= cylinder_radius**2

# Create the hollow sphere
sphere = ((x - center_x)**2 + (y - center_y)**2 + (z - center_z)**2) <= sphere_radius**2

# Combine the cylinder and the hollow sphere
combined = cylinder & ~sphere

# Convert the data type to uint8
combined = combined.astype(np.uint8)

# Adjust dimensions for Matlab compatibility
combined = np.transpose(combined, (2, 1, 0))

combined = combined.astype('<u1')
# Save the volume as a .npy file with little endian byte order
np.save('cylinder_with_hollow_sphere_uint8.npy', combined)

print("3D volume saved as 'cylinder_with_hollow_sphere_uint8.npy'")
