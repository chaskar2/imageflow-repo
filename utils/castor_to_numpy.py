import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.widgets import Slider
from PIL import Image


vector_of_data = np.fromfile('../output/castor_recon_output_it8.img ', dtype=np.float32)
stack_of_images = vector_of_data.reshape((260,260,260))

stack_of_images = stack_of_images[:, 115:145, 115:145]

fig = plt.figure()

idx0 = 130
l = plt.imshow(stack_of_images[idx0])


axidx = plt.axes([0.25, 0, 0.5, 0.10 ])
slidx = Slider(axidx, 'index', 0, 259, valinit=idx0, valfmt='%d')

def update(val):
    idx = slidx.val
    l.set_data(stack_of_images[int(idx)])
    fig.canvas.draw_idle()
    
slidx.on_changed(update)

plt.title('Castor Recon', color='red')
plt.show()



def save_image(image, filename):
    formatted = (image * 255 / np.max(image)).astype('uint8')
    im = Image.fromarray(formatted)
    im.save(filename)


# for i in range(260):
#     print(f'Saving image {i}')
#     save_image(stack_of_images[i], f'./reconstructed_images/image_{i}.png')
