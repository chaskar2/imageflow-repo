#!/bin/bash

hadd -f merged_output.root ./Output/output_*.root

castor-GATERootToCastor -i ./merged_output.root -o castor_output.dat -m main.mac -geo -s CastorFormattedGeometry

castor-recon -df castor_output.dat_df.Cdh -it 8:4 -dim 260,260,260 -fout castor_recon_output -th 2

rm -f output_*