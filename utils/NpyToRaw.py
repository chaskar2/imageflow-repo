import numpy as np
import scipy.io

img = np.load('cylinder_with_hollow_sphere_uint8.npy', allow_pickle=True, mmap_mode='r')
img = img.astype('<u1')
img.tofile('py_cylinder_with_hollow_sphere_uint8.raw')
