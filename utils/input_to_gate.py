import numpy as np
import pydicom
from pydicom.dataset import Dataset, FileMetaDataset
from pydicom.uid import generate_uid, ExplicitVRLittleEndian
import os
import matplotlib.pyplot as plt


# Dimensions
x_size, y_size, z_size = 300, 300, 400
radius_outer_sphere = 100
radius_inner_hole = 50

# Create an empty 3D array (initially filled with zeros)
volume = np.zeros((x_size, y_size, z_size), dtype=np.uint8)

# Center of the sphere
center_x, center_y, center_z = x_size // 2, y_size // 2, z_size // 2

# Create the cylindrical box
for z in range(z_size):
    for y in range(y_size):
        for x in range(x_size):
            # Calculate distance from the center
            distance_from_center = np.sqrt((x - center_x)**2 + (y - center_y)**2 + (z - center_z)**2)

            # Define the outer sphere
            if distance_from_center <= radius_outer_sphere:
                # Define the inner hollow sphere
                if distance_from_center >= radius_inner_hole:
                    volume[x, y, z] = 255

# Save the volume as a .npy file
np.save("volume.npy", volume)



# # Create the directory if it doesn't exist
# output_dir = "images"
# os.makedirs(output_dir, exist_ok=True)

# # Save each z axis slice as an image
# for z in range(z_size):
#     # Create a figure and plot the slice
#     fig, ax = plt.subplots()
#     ax.imshow(volume[:, :, z], cmap='gray')
#     ax.axis('off')

#     # Save the figure as an image
#     output_filename = os.path.join(output_dir, f"slice_{z}.png")
#     plt.savefig(output_filename, bbox_inches='tight', pad_inches=0)

#     # Close the figure to free up memory
#     plt.close(fig)

# # Now volume contains the 3D cylindrical box with the hollow sphere


# # Create File Meta Information
# file_meta = FileMetaDataset()
# file_meta.MediaStorageSOPClassUID = pydicom.uid.SecondaryCaptureImageStorage
# file_meta.MediaStorageSOPInstanceUID = generate_uid()
# file_meta.ImplementationClassUID = generate_uid()

# # Create the DICOM dataset
# ds = Dataset()
# ds.file_meta = file_meta

# # Add required DICOM tags
# ds.PatientName = "Test^Patient"
# ds.PatientID = "123456"
# ds.StudyInstanceUID = generate_uid()
# ds.SeriesInstanceUID = generate_uid()
# ds.SOPInstanceUID = file_meta.MediaStorageSOPInstanceUID
# ds.SOPClassUID = file_meta.MediaStorageSOPClassUID
# ds.Modality = "OT"  # Other
# ds.StudyID = "1"
# ds.SeriesNumber = "1"
# ds.InstanceNumber = "1"
# ds.ImagePositionPatient = [0, 0, 0]
# ds.ImageOrientationPatient = [1, 0, 0, 0, 1, 0]
# ds.PatientPosition = "HFS"  # Head First Supine
# ds.Rows, ds.Columns = x_size, y_size
# ds.SliceThickness = 1
# ds.PixelSpacing = [1.0, 1.0]  # Assuming isotropic spacing

# # Set the transfer syntax
# ds.file_meta.TransferSyntaxUID = ExplicitVRLittleEndian

# # Add pixel data
# ds.PixelData = volume.tobytes()
# ds.BitsAllocated = 8
# ds.BitsStored = 8
# ds.HighBit = 7
# ds.SamplesPerPixel = 1
# ds.PhotometricInterpretation = "MONOCHROME2"

# # Save the dataset to a DICOM file
# output_filename = "output_3d.dcm"
# pydicom.dcmwrite(output_filename, ds)
