import numpy as np
import matplotlib.pyplot as plt

# Define the dimensions of the cube
cube_size = 200

# Create a 3D array filled with zeros (background)
volume = np.zeros((cube_size, cube_size, cube_size))

# Cylinder parameters
cylinder_height = 100
cylinder_radius = 50

# Center of the cube
center_x, center_y, center_z = cube_size // 2, cube_size // 2, cube_size // 2

# Define the bounds of the cylinder along the z-axis
z_min = center_z - cylinder_height // 2
z_max = center_z + cylinder_height // 2

# Iterate through each point in the 3D array
for x in range(cube_size):
    for y in range(cube_size):
        # Check if the point is within the radius of the cylinder in the x-y plane
        if (x - center_x)**2 + (y - center_y)**2 <= cylinder_radius**2:
            # Set the points within the cylinder height range to 1
            volume[x, y, z_min:z_max] = 1

# # Visualize a few slices of the volume
# fig, axes = plt.subplots(1, 3, figsize=(15, 5))

# # Slice through the center of the cylinder
# axes[0].imshow(volume[center_x, :, :], cmap='gray')
# axes[0].set_title('Slice through center (XY plane)')

# axes[1].imshow(volume[:, center_y, :], cmap='gray')
# axes[1].set_title('Slice through center (XZ plane)')

# axes[2].imshow(volume[:, :, center_z], cmap='gray')
# axes[2].set_title('Slice through center (YZ plane)')

# plt.show()


volume = volume.reshape((200, 200, 200), order='F')
# Save the volume as a .npy file
np.save("cube.npy", volume)
