import os
import numpy as np
from PIL import Image

# Directory containing the images
image_dir = "images"

# Get list of image files
image_files = sorted([os.path.join(image_dir, f) for f in os.listdir(image_dir) if f.endswith('.png')])

# Read the first image to get the dimensions
sample_image = Image.open(image_files[0])
width, height = sample_image.size

# Initialize an empty list to hold the image arrays
image_stack = []

# Read each image and append it to the list
for image_file in image_files:
    image = Image.open(image_file).convert('L')  # Convert to grayscale
    image_array = np.array(image)
    image_stack.append(image_array)

# Stack the list into a 3D NumPy array
image_stack = np.stack(image_stack, axis=-1)

#### Step 2: Save the 3D NumPy Array as a .RAW File

# Save the 3D array as a .RAW file
raw_filename = "output_3d.raw"
image_stack.tofile(raw_filename)
