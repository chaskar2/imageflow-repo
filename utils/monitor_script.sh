#!/bin/bash

# Usage ./monitor_script.sh output_file.txt 

# Get the PID of Gate main.mac
pid=$(pgrep -f "Gate main.mac")
metrics_filename=$1

while [ -e /proc/$pid ]
do
    echo "Timestamp: $(date)" >> $metrics_filename
    echo "Runtime Metrics:" >> $metrics_filename
    top -b -n 1 | head -5 >> $metrics_filename
    echo "Disk Utilization:" >> $metrics_filename
    df -h >> $metrics_filename
    echo "Memory Utilization:" >> $metrics_filename
    free -h >> $metrics_filename
    echo "CPU Metrics:" >> $metrics_filename
    vmstat 1 5 >> $metrics_filename
    echo "=====================================" >> $metrics_filename
    sleep 600
done