from airflow import DAG
from datetime import datetime
from airflow.operators.python  import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.providers.docker.operators.docker import DockerOperator
from airflow.decorators import task, dag
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from kubernetes.client import models as k8s



@dag(start_date=datetime(2021, 1,1), schedule_interval='@daily', catchup=False)
def kubernetes_dag_simple_sharing():

    get_scan_parameters_task = KubernetesPodOperator(
    name="get_scan_parameters_task",
    image="debian",
    cmds=["sh", "-c", "mkdir -p /airflow/xcom/;echo '[1,2,3,4]' > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="get_scan_parameters_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    embed_objects_task = KubernetesPodOperator(
    name="embed_objects_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('get_scan_parameters_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('get_scan_parameters_task') }} > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="embed_objects_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    collate_scan_parameters_task = KubernetesPodOperator(
    name="collate_scan_parameters_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('embed_objects_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('get_scan_parameters_task') }} > /airflow/xcom/return.json"],
    task_id="collate_scan_parameters_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    virtual_imaging_task = KubernetesPodOperator(
    name="virtual_imaging_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('collate_scan_parameters_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('collate_scan_parameters_task') }} > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="virtual_imaging_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    image_reconstruction_task = KubernetesPodOperator(
    name="image_reconstruction_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('virtual_imaging_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('virtual_imaging_task') }} > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="image_reconstruction_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    image_processing_task = KubernetesPodOperator(
    name="image_processing_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('image_reconstruction_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('image_reconstruction_task') }} > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="image_processing_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    analysis_and_error_checking_task = KubernetesPodOperator(
    name="analysis_and_error_checking_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('image_processing_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('image_processing_task') }} > /airflow/xcom/return.json"],
    labels={"app": "imageflow"},
    task_id="analysis_and_error_checking_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    collate_and_notify_task = KubernetesPodOperator(
    name="collate_and_notify_task",
    image="debian",
    cmds=["sh", "-c", "echo {{ task_instance.xcom_pull('analysis_and_error_checking_task') }}; mkdir -p /airflow/xcom/; echo {{ task_instance.xcom_pull('analysis_and_error_checking_task') }} > /airflow/xcom/return.json"],
    arguments=["echo", "collate_and_notify_task running"],
    labels={"app": "imageflow"},
    task_id="collate_and_notify_task",
    on_finish_action="delete_pod",
    in_cluster=True,
    do_xcom_push=True,
    )

    get_scan_parameters_task >> embed_objects_task >> collate_scan_parameters_task >> virtual_imaging_task >> image_reconstruction_task >> image_processing_task >> analysis_and_error_checking_task >> collate_and_notify_task

# dag = kubernetes_dag_simple_sharing()

