import os
import json
from collections import defaultdict
import time
from datetime import datetime
from pathlib import Path

import scipy
import skimage
import numpy as np

from airflow.decorators import task, dag
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from kubernetes.client import models as k8s
from airflow.models import Variable






@dag(start_date=datetime(2021, 1,1), schedule_interval='@daily', catchup=False)
def kubernetes_dag_template(**dag_input):
    # Setting up volume to share data across airflow tasks pre image reconstruction
    executor_config_volume_mount = {
            "pod_override": k8s.V1Pod(
                spec=k8s.V1PodSpec(
                    containers=[
                        k8s.V1Container(
                            name="base",
                            volume_mounts=[
                                k8s.V1VolumeMount(mount_path="/scan_params/", name="imageflow-disk-volume")
                            ],
                            security_context=k8s.V1SecurityContext(run_as_user=0)
                        )
                    ],
                    volumes=[
                        k8s.V1Volume(
                            name="imageflow-disk-volume",
                            persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="imageflow-disk"),
                        )
                    ],
                )
            )
        }
    
    # Setting up volume to share data across airflow tasks post image reconstruction
    executor_config_volume_mount_castor = {
            "pod_override": k8s.V1Pod(
                spec=k8s.V1PodSpec(
                    containers=[
                        k8s.V1Container(
                            name="base",
                            volume_mounts=[
                                k8s.V1VolumeMount(mount_path="/castor_files/", name="castor-disk-volume")
                            ],
                            security_context=k8s.V1SecurityContext(run_as_user=0)
                        )
                    ],
                    volumes=[
                        k8s.V1Volume(
                            name="castor-disk-volume",
                            persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="castor-disk"),
                        )
                    ],
                )
            )
        }
    
    # Create gate volume and volume mounts
    gate_volume = k8s.V1Volume(name="imageflow-gate-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="gate-disk"))
    gate_volume_mount = k8s.V1VolumeMount(mount_path="/gate_files", name="imageflow-gate-volume")

    # Create castor volume and volume mounts
    castor_volume = k8s.V1Volume(name="imageflow-castor-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="castor-disk"))
    castor_volume_mount = k8s.V1VolumeMount(mount_path="/castor_files", name="imageflow-castor-volume")


    def embed(small_array, big_array, big_index):
        '''
        Function to embed smaller 3d array into a large 3d array
        '''
        big_array[big_index[0]:big_index[0]+small_array.shape[0], \
                big_index[1]:big_index[1]+small_array.shape[1], \
                big_index[2]:big_index[2]+small_array.shape[2]] = small_array

        return
    

    def prepare_current_scan_config(scan_config, body_choice, tumor_position_choice, tumor_size_choice, cuts):
        '''
        Function to prepare scan configuration files
        '''
        curr_scan_config = defaultdict(dict)
        
        curr_scan_config["body"] = scan_config["BODY_CONFIG"][body_choice]
        
        curr_scan_config["tumor"]["tumor_d"] = scan_config["TUMOR_SIZE_CONFIG"][tumor_size_choice]["tumor_d"]
        curr_scan_config["tumor"]["tumor_w"] = scan_config["TUMOR_SIZE_CONFIG"][tumor_size_choice]["tumor_w"]
        curr_scan_config["tumor"]["tumor_l"] = scan_config["TUMOR_SIZE_CONFIG"][tumor_size_choice]["tumor_l"]

        tumor_loc_i = scan_config["TUMOR_POSITION_CONFIG"][tumor_position_choice]["tumor_loc_x"]
        tumor_loc_j = scan_config["TUMOR_POSITION_CONFIG"][tumor_position_choice]["tumor_loc_y"]
        tumor_loc_k = scan_config["TUMOR_POSITION_CONFIG"][tumor_position_choice]["tumor_loc_z"]


        tumor_loc_x = int((curr_scan_config["body"]["body_d"]*tumor_loc_i/cuts))
        tumor_loc_y = int((curr_scan_config["body"]["body_d"]*tumor_loc_j/cuts))
        tumor_loc_z = int((curr_scan_config["body"]["body_d"]*tumor_loc_k/cuts))

        curr_scan_config["tumor"]["tumor_loc_x"] = tumor_loc_x
        curr_scan_config["tumor"]["tumor_loc_y"] = tumor_loc_y
        curr_scan_config["tumor"]["tumor_loc_z"] = tumor_loc_z

        return dict(curr_scan_config)
        


    @task(executor_config=executor_config_volume_mount)
    def get_scan_parameters_task(**kwargs):
        """
        Function to fetch the user-selected configuration
        """

        run_id = time.strftime("%Y-%m-%d--%H-%M-%S", time.gmtime())
        base_path = "/scan_params/" + run_id
        Variable.set(key="base_path", value=base_path)

        if not os.path.exists(f"{Variable.get('base_path')}"):
            os.mkdir(f"{Variable.get('base_path')}")

        with open("/scan_params/scan_config.json", "r") as conf:
                scan_config = json.load(conf)

        body_choice = kwargs['dag_run'].conf.get('body_choice')
        tumor_position_choice = kwargs['dag_run'].conf.get('tumor_position_choice')
        tumor_size_choice = kwargs['dag_run'].conf.get('tumor_size_choice') 

        curr_conf_dict = prepare_current_scan_config(scan_config, body_choice, tumor_position_choice, tumor_size_choice, 3)

        with open(f"{Variable.get('base_path')}/curr_scan_config.json", "w+") as curr_conf:
                json.dump(curr_conf_dict, curr_conf)

        return 0
        
    @task(executor_config=executor_config_volume_mount)
    def embed_objects_task():
        """
        Function to embed the tumor object inside a larger body object
        """
        
        with open(f"{Variable.get('base_path')}/curr_scan_config.json", "r") as curr_conf:
            curr_conf_dict = json.load(curr_conf)

        body = curr_conf_dict["body"]
        tumor = curr_conf_dict["tumor"]

        body_d = body["body_d"]
        body_w = body["body_w"]
        body_l = body["body_l"]

        tumor_d = tumor["tumor_d"]
        tumor_w = tumor["tumor_w"]
        tumor_l = tumor["tumor_l"]

        tumor_loc_x = tumor["tumor_loc_x"]
        tumor_loc_y = tumor["tumor_loc_y"]
        tumor_loc_z = tumor["tumor_loc_z"]

        body = np.zeros((body_d, body_w, body_l))
        tumor = np.ones((tumor_d, tumor_w, tumor_l))

        embed(tumor, body, np.array((tumor_loc_z, tumor_loc_x, tumor_loc_y)))

        with open(f"{Variable.get('base_path')}/embeded_object.npy", "wb+") as writer:
            np.save(writer, body)
            print("File written successfully")
        
        return 0
    
    @task(executor_config=executor_config_volume_mount)
    def collate_scan_parameters_task():
        """
        Function to embed the embedded body-tumor object into a larger space to pass it to the GATE simulation software
        """

        #TODO: Larger Outer Box, Inner Box, Inner Sphere Tumor

        
        embeded_object = np.load(f"{Variable.get('base_path')}/embeded_object.npy")
        print("File read successfully")

        space_d = 32
        space_w = 32
        space_l = 32

        space = np.zeros((space_d, space_w, space_l))

        embed(embeded_object, space, np.array((space_d//4, space_w//4, space_l//4)))

        with open(f"{Variable.get('base_path')}/scan_parameters.npy", "wb+") as writer:
            np.save(writer, space)
            print("File written successfully")

        return 0
        

    gate_virtual_imaging_task = KubernetesPodOperator(
    namespace="airflow",
    image="opengatecollaboration/gate:9.3-docker",
    name="gate_virtual_imaging_task",
    task_id="gate_virtual_imaging_task",
    is_delete_operator_pod=True,
    volumes=[gate_volume],
    volume_mounts=[gate_volume_mount],
    cmds=["/gate_files/gate_runner/runGate.sh"],
    arguments=["/gate_files/imaging/PET", "PET_CylindricalPET_System.mac"], 
    get_logs=True,
    )

    castor_image_reconstruction_task = KubernetesPodOperator(
    namespace="airflow",
    image="saumitra9946/castor-v3.1.1:2",
    name="castor_image_reconstruction_task",
    task_id="castor_image_reconstruction_task",
    is_delete_operator_pod=True,
    volumes=[gate_volume, castor_volume],
    volume_mounts=[gate_volume_mount, castor_volume_mount],
    cmds=["/castor_files/castor_recon_from_gate.sh"],
    arguments=["YourFile.root", "CastorAirflowRecon.dat", "PET_CylindricalPET_System.mac" ,"CastorAirflowReconOutput"], 
    get_logs=True,
    )


    def get_image_statistics(image_3d):
        """
        get_image_statistics: Get layer wise mean values for the volume
        """
        img_mean_stats = []
        img_sd_stats = []

        # calculate mean and standard deviation by layers
        for i in range(10):
            img_mean_stats.append(np.mean(image_3d[i,:,:]))
            img_sd_stats.append(np.std(image_3d[i,:,:]))
            
        return img_mean_stats, img_sd_stats
    
    @task(executor_config=executor_config_volume_mount_castor)
    def error_checking_task():
        """
        Tests whether the volume has been mounted.
        """

        # TODO: Check consistency existing results

        vector_of_data = np.fromfile('/castor_files/output/CastorAirflowReconOutput_it8.img', dtype=np.float32)
        
        try:
            stack_of_images = vector_of_data.reshape((10,10,10))
            print(stack_of_images.shape)
            print("Reshaping was succesful ...")
            return 0
        except Exception:
            print("ERROR: Reshaping image ... Check reconstruction")
            return 1



    @task(executor_config=executor_config_volume_mount_castor)
    def image_processing_task():
        """
        Image statistics derivation
        """

        vector_of_data = np.fromfile('/castor_files/output/CastorAirflowReconOutput_it8.img', dtype=np.float32)
        stack_of_images = vector_of_data.reshape((10,10,10))
        
        img_mean_stats, img_sd_stats = get_image_statistics(stack_of_images)

        with open('/castor_files/stats_output/img_statistics.csv','a') as fd:
            fd.write(f"{Variable.get('base_path')},{img_mean_stats},{img_sd_stats}\n")
            print("Statistics written to file")

            return 0
    
    
    @task
    def collate_and_notify_task():
        """
        Tests whether the volume has been mounted.
        """

        # TODO: Notify the user

        print("Pipeline execution completed ...")

        print("Simmulating notification")

        return 0

    
    get_scan_parameters = get_scan_parameters_task()
    embed_objects = embed_objects_task()
    collate_scan_parameters = collate_scan_parameters_task()
    image_processing = image_processing_task() 
    error_checking = error_checking_task() 
    collate_and_notify = collate_and_notify_task()

    get_scan_parameters >> embed_objects >> collate_scan_parameters >> gate_virtual_imaging_task >> castor_image_reconstruction_task >> error_checking >> image_processing >> collate_and_notify

dag = kubernetes_dag_template()
