from airflow import DAG
from datetime import datetime
from airflow.operators.python  import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.providers.docker.operators.docker import DockerOperator
from airflow.decorators import task, dag
import json
import os



@dag(start_date=datetime(2021, 1,1), schedule_interval='@daily', catchup=False)
def simple_dag():

    def get_scan_parameters(**kwargs):
        conf_id = kwargs['dag_run'].conf.get('conf_id')    
        print("CONF ID", conf_id) 

        os.chdir("/mnt/c/Users/somic/Work/Research/pipeline/src")

        print(os.listdir("."))

        with open("../inputs/scan_parameters.json") as f, open("../containers/embed_objects/scan_parameter_conf.json", "w") as wi:
            scan_parameters = json.load(f)
            scan_parameter_conf = scan_parameters[conf_id]
            print(scan_parameter_conf)
            json.dump(scan_parameter_conf, wi)

        return

    def collate_parameters(**context):
        data = context['task_instance'].xcom_pull(task_ids=context['task'].upstream_task_ids, key='return_value')
        print(type(data))

        return data

    get_scan_parameters = PythonOperator(
        task_id='get_scan_parameters',
        provide_context=True,
        python_callable=get_scan_parameters
    )


    embed_objects = DockerOperator(
        task_id = 'embed_objects',
        image = 'embed_objects:v1',
        network_mode = 'bridge',
        docker_url='unix:///var/run/docker.sock',
        auto_remove = True,
        xcom_all = True,
        host_tmp_dir = '/mnt/c/Users/somic/Work/Research/pipeline/containers/embed_objects'
        
    )


    collate_parameters = PythonOperator(
        task_id = "collate_parameter",
        provide_context = True,
        python_callable = collate_parameters
            )

    get_scan_parameters >> embed_objects >> collate_parameters

# dag = simple_dag()

