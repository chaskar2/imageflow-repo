import os
import json
from collections import defaultdict
import time
from datetime import datetime, timedelta
from pathlib import Path
import re
import shutil
from datetime import timedelta




import scipy
import skimage
import numpy as np

from airflow.decorators import task, dag
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from airflow.operators.python import PythonOperator
from kubernetes.client import models as k8s
from airflow.models import Variable
import os
from airflow.operators.dummy_operator import DummyOperator

def parse_macro(macro_file_path):
    # Function to parse a macro file and return its content as a list of lists
    
    with open(macro_file_path) as f:
        content = f.readlines()

    ans = []
    
    for line in content:
        line_processed = re.sub(r'\s', ' ', line)  # Replace consecutive whitespaces with a single whitespace
        line_processed = line_processed.strip()  # Remove leading and trailing whitespaces
        line_processed = re.sub(r'\s+', ' ', line_processed)  # Replace multiple whitespaces with a single whitespace
        line_processed = line_processed.split(' ', 1)  # Split the line into two parts at the first occurrence of a whitespace
        
        ans.append(line_processed)
        
    return ans


def convert_list_to_macro(macro_property_list, macro_file_path):
    # Function to convert a list of macro properties into a macro file
    
    with open(macro_file_path, 'w') as f:
        for macro_property in macro_property_list:
            f.write(' '.join(map(str, macro_property)) + '\n')  # Write each macro property as a line in the macro file

    return True


def set_macro_property(macro_file_path, property_name, property_value, output_macro_file_path):
    # Function to set the value of a macro property in a macro file
    
    macro_properties = parse_macro(macro_file_path)  # Parse the macro file and get the list of macro properties
    
    for property in macro_properties:
        if len(property) == 2 and property[0] == property_name:
            property[1] = property_value  # Update the value of the specified macro property

    return convert_list_to_macro(macro_properties, output_macro_file_path)  # Convert the updated macro properties list back to a macro file


def replace_macro_property_value(macro_file_path, property_name, original_property_value, property_value, output_macro_file_path):
    # Function to replace the value of a macro property in a macro file
    
    macro_properties = parse_macro(macro_file_path)  # Parse the macro file and get the list of macro properties
    
    for property in macro_properties:
        if len(property) == 2 and property[0] == property_name and property[1] == original_property_value:
            property[1] = property_value  # Replace the value of the specified macro property

    return convert_list_to_macro(macro_properties, output_macro_file_path)  # Convert the updated macro properties list back to a macro file


# Define a Python function to introduce delay
def introduce_delay(delay_seconds):
    time.sleep(delay_seconds)



default_args = {
    'retries': 3,  # Number of retries
    'retry_delay': timedelta(minutes=5),  # Delay between retries
    # 'execution_timeout': timedelta(minutes=60),  # Maximum time the task can run
}



@dag(start_date=datetime(2021, 1,1), schedule_interval='@daily', catchup=False, default_args=default_args,)
def imageflow_dag(**dag_input):

    
    # Create gate volume and volume mounts
    gate_volume = k8s.V1Volume(name="imageflow-gate-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="gate-disk"))
    gate_volume_mount = k8s.V1VolumeMount(mount_path="/gate_files", name="imageflow-gate-volume")

    # Create gate output volume and volume mounts
    gate_output_volume = k8s.V1Volume(name="imageflow-gate-output-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="gate-output-disk"))
    gate_output_volume_mount = k8s.V1VolumeMount(mount_path="/gate_output", name="imageflow-gate-output-volume")

    # Create gate output volume and volume mounts for storing output files from the airflow pipeline
    gate_output_pipeline_volume = k8s.V1Volume(name="imageflow-gate-output-pipeline-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="gate-output-pipeline-disk"))
    gate_output_pipeline_volume_mount = k8s.V1VolumeMount(mount_path="/gate_output_pipeline", name="imageflow-gate-output-pipeline-volume")

    # Create scan parameters volume and volume mounts
    gate_scan_params_volume = k8s.V1Volume(name="imageflow-disk-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="imageflow-disk"))
    gate_scan_params_volume_mount = k8s.V1VolumeMount(mount_path="/scan_params", name="imageflow-disk-volume")

    # Create castor volume and volume mounts
    castor_volume = k8s.V1Volume(name="imageflow-castor-volume",persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="castor-disk"))
    castor_volume_mount = k8s.V1VolumeMount(mount_path="/castor_files", name="imageflow-castor-volume")

    # Setting up volume to share data across airflow tasks pre image reconstruction
    executor_config_volume_mount = {
        "pod_override": k8s.V1Pod(
            spec=k8s.V1PodSpec(
                containers=[
                    k8s.V1Container(
                        name="base",
                        volume_mounts=[
                            gate_scan_params_volume_mount,
                            gate_output_volume_mount,
                            gate_output_pipeline_volume_mount,
                            castor_volume_mount
                        ],
                        security_context=k8s.V1SecurityContext(run_as_user=0)
                    )
                ],
                volumes=[
                    gate_scan_params_volume,
                    gate_output_volume,
                    gate_output_pipeline_volume,
                    castor_volume
                ]
            )
        )
    }
    
    
    # Setting up volume to share data across airflow tasks post image reconstruction
    executor_config_volume_mount_castor = {
            "pod_override": k8s.V1Pod(
                spec=k8s.V1PodSpec(
                    containers=[
                        k8s.V1Container(
                            name="base",
                            volume_mounts=[
                                castor_volume_mount,
                                gate_output_pipeline_volume_mount
                            ],
                            security_context=k8s.V1SecurityContext(run_as_user=0)
                        )
                    ],
                    volumes=[
                        castor_volume,
                        gate_output_pipeline_volume
                    ],
                )
            )
        }
    
    def prepare_current_scan_config(scan_config, tumor_activity_choice):
        '''
        Function to prepare scan configuration files
        '''
        curr_scan_config = defaultdict(dict)
        tumor_activity_file = scan_config["TUMOR_ACTIVITY_CONFIG"][tumor_activity_choice]

        curr_scan_config["activity_file"] = tumor_activity_file

        return dict(curr_scan_config)
        


    @task(executor_config=executor_config_volume_mount)
    def get_scan_parameters_task(**kwargs):
        """
        Function to fetch the user-selected configuration

        Args:
            **kwargs: Additional keyword arguments

        Returns:
            int: The return value indicating the success of the function (always 0 in this case)
        """
        
        base_path = "/scan_params/" + kwargs['dag_run'].conf.get('run_id') 
        base_output_path = "/gate_output_pipeline/" + kwargs['dag_run'].conf.get('run_id') 
        
        Variable.set(key="base_path", value=base_path)
        Variable.set(key="base_output_path", value=base_output_path)
        

        # Create the base path directory if it doesn't exist
        if not os.path.exists(f"{Variable.get('base_path', 'placeholder')}"):
            os.mkdir(f"{Variable.get('base_path', 'placeholder')}")

        # Load the scan configuration from the file
        with open("/scan_params/scan_config.json", "r") as conf:
            scan_config = json.load(conf)

        # Get the user-selected tumor activity choice from the DAG run configuration
        tumor_activity_choice = kwargs['dag_run'].conf.get('tumor_activity_choice') 

        # Prepare the current scan configuration based on the user-selected tumor activity choice
        curr_conf_dict = prepare_current_scan_config(scan_config, tumor_activity_choice)

        # Save the current scan configuration to a file
        with open(f"{Variable.get('base_path', 'placeholder')}/curr_scan_config.json", "w+") as curr_conf:
            print(f"Directory scan params written to: {Variable.get('base_path', 'placeholder')}")
            json.dump(curr_conf_dict, curr_conf)

        return 0
        
    @task(executor_config=executor_config_volume_mount)
    def embed_objects_task():
        """
        Function to embed the tumor object inside a larger body object
        """
        
        # TODO: Copy all macros file data files

        shutil.copytree('/gate_output/NCSA_RubInWater/Macros', f"{Variable.get('base_path', 'placeholder')}/Macros")
        shutil.copytree('/gate_output/NCSA_RubInWater/Data', f"{Variable.get('base_path', 'placeholder')}/Data")
        shutil.copy('/gate_output/NCSA_RubInWater/main.mac', f"{Variable.get('base_path', 'placeholder')}/main.mac")

        return 0



    
    @task(executor_config=executor_config_volume_mount)
    def collate_scan_parameters_task():
        """
        Function to embed the embedded body-tumor object into a larger space to pass it to the GATE simulation software
        """

        # TODO: Replace the macros with user selected values

        # Open the current scan configuration file
        with open(f"{Variable.get('base_path', 'placeholder')}/curr_scan_config.json", "r+") as curr_conf:
            curr_conf = json.load(curr_conf)
        
        # Set the activity file property for the GATE simulation software
        activity_file_property = "/gate/source/voxel_blob/imageReader/rangeTranslator/readTable"
        activity_file_value = curr_conf["activity_file"]
        set_macro_property(f"{Variable.get('base_path', 'placeholder')}/Macros/VoxelizedSource.mac", activity_file_property, activity_file_value, f"{Variable.get('base_path', 'placeholder')}/Macros/VoxelizedSource.mac")

        main_mac_file = f"{Variable.get('base_path', 'placeholder')}/main.mac"

        for component in range(20):
            # Set the output file property for the GATE simulation software
            
            new_mac_file = f"{Variable.get('base_path', 'placeholder')}/main_{component}.mac"

            output_mac_file_property = "/control/execute"
            original_output_file_value = "Macros/Output.mac"
            output_mac_file_value = f"Macros/Output_{component}.mac"
            replace_macro_property_value(main_mac_file, output_mac_file_property, original_output_file_value, output_mac_file_value, new_mac_file)

            output_file_property = "/gate/output/root/setFileName"
            output_file_value = f"Output/output_{component}.root"
            set_macro_property(f"{Variable.get('base_path', 'placeholder')}/Macros/Output.mac", output_file_property, output_file_value, f"{Variable.get('base_path', 'placeholder')}/Macros/Output_{component}.mac")

        
        # Create the Output directory if it doesn't exist
        if not os.path.exists(f"{Variable.get('base_path', 'placeholder')}/Output"):
            os.makedirs(f"{Variable.get('base_path', 'placeholder')}/Output")

        return 0



    @task(executor_config=executor_config_volume_mount)
    def move_pipeline_output_task(executor_config=executor_config_volume_mount):
        """
        Function to move the output files from the scan_params directory to the output directory
        """

        if os.path.exists(Variable.get('base_output_path', 'placeholder')):
            shutil.rmtree(Variable.get('base_output_path', 'placeholder'))
            print(f"Deleted folder: {Variable.get('base_output_path', 'placeholder')}")
        else:
            print(f"Folder does not exist: {Variable.get('base_output_path', 'placeholder')}")

        # Move the output files from the pipeline to the output directory
        shutil.copytree(Variable.get('base_path', 'placeholder'), Variable.get('base_output_path', 'placeholder'))

        # Remove the pipeline output directory  
        # shutil.rmtree(Variable.get('base_path', 'placeholder'))

        # Copy the castor reconstruction script to the output directory
        shutil.copy("/castor_files/castor_reconstruction.sh", Variable.get('base_output_path', 'placeholder'))

        return 0
    
    

    castor_image_reconstruction_task = KubernetesPodOperator(
    namespace="airflow",
    image="saumitra9946/castor-v3.1.1:2",
    name="castor_image_reconstruction_task",
    task_id="castor_image_reconstruction_task",
    is_delete_operator_pod=True,
    volumes=[gate_volume, castor_volume, gate_output_pipeline_volume],
    volume_mounts=[gate_volume_mount, castor_volume_mount, gate_output_pipeline_volume_mount],
    cmds=[f"{Variable.get('base_output_path', 'placeholder')}/castor_reconstruction.sh"],
    arguments=[f"{Variable.get('base_output_path', 'placeholder')}"],
    security_context = {"runAsNonRoot": False},
    get_logs=True,
    execution_timeout = timedelta(minutes=60)
    )


    def get_image_statistics(image_3d):
        """
        get_image_statistics: Get layer wise mean values for the volume
        """
        # Remove the output.root file from the base path
        os.remove(f"{Variable.get('base_path', 'placeholder')}/Output/output.root.root")


        img_mean_stats = []
        img_sd_stats = []

        # calculate mean and standard deviation by layers
        for i in range(10):
            img_mean_stats.append(np.mean(image_3d[i,:,:]))
            img_sd_stats.append(np.std(image_3d[i,:,:]))
            
        return img_mean_stats, img_sd_stats
    
    @task(executor_config=executor_config_volume_mount_castor)
    def error_checking_task():
        """
        Tests whether the volume has been mounted.
        """

        # TODO: Check consistency existing results

        vector_of_data = np.fromfile('/castor_files/output/CastorAirflowReconOutput_it8.img', dtype=np.float32)
        
        try:
            stack_of_images = vector_of_data.reshape((10,10,10))
            print(stack_of_images.shape)
            print("Reshaping was succesful ...")
            return 0
        except Exception:
            print("ERROR: Reshaping image ... Check reconstruction")
            return 1



    @task(executor_config=executor_config_volume_mount_castor)
    def image_processing_task():
        """
        Image statistics derivation
        """

        vector_of_data = np.fromfile('/castor_files/output/CastorAirflowReconOutput_it8.img', dtype=np.float32)
        stack_of_images = vector_of_data.reshape((10,10,10))
        
        img_mean_stats, img_sd_stats = get_image_statistics(stack_of_images)

        with open('/castor_files/stats_output/img_statistics.csv','a') as fd:
            fd.write(f"{Variable.get('base_path', 'placeholder')},{img_mean_stats},{img_sd_stats}\n")
            print("Statistics written to file")

            return 0
    
    
    @task
    def collate_and_notify_task():
        """
        Tests whether the volume has been mounted.
        """

        # TODO: Notify the user

        print("Pipeline execution completed ...")

        print("Simmulating notification")

        return 0


    get_scan_parameters = get_scan_parameters_task()
    embed_objects = embed_objects_task()
    collate_scan_parameters = collate_scan_parameters_task()
    move_pipeline_output = move_pipeline_output_task()
    # image_processing = image_processing_task() 
    # error_checking = error_checking_task() 
    # collate_and_notify = collate_and_notify_task()

    get_scan_parameters >> embed_objects >> collate_scan_parameters


    gate_virtual_imaging_tasks = []
    
    for i in range(20):
        gate_virtual_imaging_tasks.append(KubernetesPodOperator(
        namespace="airflow",
        image="opengatecollaboration/gate:9.2-docker",
        name=f"gate_virtual_imaging_task_{i}",
        task_id=f"gate_virtual_imaging_task_{i}",
        labels={"pod_type": "gate_simulator"},
        is_delete_operator_pod=False,
        volumes=[gate_volume, gate_output_volume, gate_scan_params_volume, gate_output_pipeline_volume],
        volume_mounts=[gate_volume_mount, gate_output_volume_mount, gate_scan_params_volume_mount, gate_output_pipeline_volume_mount],
        cmds=["/gate_files/gate_runner/runGate.sh"],
        arguments=[Variable.get('base_path', 'placeholder'), f"main_{i}.mac"], 
        execution_timeout = timedelta(minutes=30),
        get_logs=True
    )
    )

    prev_dummy_task = None
    dummy_task = None

    for i in range(len(gate_virtual_imaging_tasks)):
        if i%5 == 0:
            prev_dummy_task = dummy_task
            dummy_task = DummyOperator(task_id=f"dummy_task_{i//5}")


        delay_task = PythonOperator(
            task_id=f'delay_task_{i}',
            python_callable=introduce_delay,
            op_kwargs={'delay_seconds':60*(i%5)})
        

        if prev_dummy_task:
            prev_dummy_task >> delay_task >> gate_virtual_imaging_tasks[i] >> dummy_task
        else:
            collate_scan_parameters >> delay_task >> gate_virtual_imaging_tasks[i] >> dummy_task


    dummy_task >> move_pipeline_output >> castor_image_reconstruction_task

dag = imageflow_dag()

